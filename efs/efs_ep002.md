# EFS ep2 : Résistance(s)

**Épisode 2 ( 3 juin 2022 )** 

<img src="images/efs_002/01_chat_colonel.jpg " alt="chat_colonel" width="300"/>

 -  ![youtube](images/youtube.png) Youtube
     -  https://www.youtube.com/watch?v=Cx9RoBirwyk
 - ![twitch](images/twitch.png) Twitch : 
     - https://www.twitch.tv/videos/1316704331

# Introduction
## Préambule
[![youtube](images/youtube.png) (00:02:35)](https://www.youtube.com/watch?v=Cx9RoBirwyk&t=2m35s)   
Après la dernière séance très académique et théorique, cet épisode se veut plus pratique.  
La résistance est un des composants de base de l'électronique, parmi :
- R : les résistances
- L : les inductances
- C : les condensateurs
- Les semi-conducteurs
    - les diodes
    - les transistors
        - bipolaires
        - à effet de champs
        - ...

Les 3 premières briques (R, L et C s) sont capitales à la compréhension du reste

## Simplification des équations de Maxwell
[![youtube](images/youtube.png) (00:05:04)](https://www.youtube.com/watch?v=Cx9RoBirwyk&t=5m4s)  

En simplifiant le modèle complet de Maxwell, on reste sur une notion de tension (différence de potentiel) et intensité électrique. 
Les conducteurs sont supposés parfaits

Un schéma simple  :  
![circuit simple](images/efs_002/02_circuit1.png)

On a U₁ + U₂ = Uv  
Les tensions ont un sens (si on retourne la flèche, la tension devient l'opposé)

### Les conventions de notation
[![youtube](images/youtube.png) (00:08:10)](https://www.youtube.com/watch?v=Cx9RoBirwyk&t=8m10s)  

Quand on est sur un dipôle "récepteur"  (c'est à  dire pas une source de tension ou courant), on s'arrange pour noter le courant dans le sens opposé à la tension, par convention.

C'est la "convention récepteur". Toutes les formules qu'on verra par la suite s'entendent dans cette convention.

Si on note les tensions dans le sens du courant (convention émetteur) alors il faudra adapter toutes les formules.

Ci-dessous, 
- en haut, la convention émetteur (pour les sources de courant et de tension)
- en bas la convention récepteur (pour le reste)

![conventions](images/efs_002/03_conventions.png)

### Potentiel 0V et "terre"
[![youtube](images/youtube.png) (00:11:49)](https://www.youtube.com/watch?v=Cx9RoBirwyk&t=11m49s) 

Par convention on relie le potentiel (-) du générateur à la "terre" et on considère ce potentiel à 0v.

Symbole de la terre (ou ground, ou gnd):   
![terre](images/efs_002/04_terre.png)

Il s'agit d'une référence par rapport à laquelle on mesure les différences de potentiel. Ce n'est pas une obligation, on peut mesurer une tension entre deux points quelconques, mais le 0V est en général à la terre.


Rappel sur le courant: 
- la différence de potentiel traduit le sens du champ électrique.
- Sa valeur traduit la "force" du champ. 
- A notre échelle, ce champ s'établit instantanément. 
- Les charges (donc le courant électrique) se contentent de suivre ce champ E. Elles sont "entrainées" par le champ. 
- on a toujours la même quantité de charge qui sort du (+) du générateur que celles qui rentrent par le (-). Il n'y a pas de "fuites"



## Utilisation des étiquettes dans les schémas complexes
[![youtube](images/youtube.png) (00:15:18)](https://www.youtube.com/watch?v=Cx9RoBirwyk&t=15m18s) 

Dans les schémas complexes, on ne relie pas toutes les terres entre elles par un fil, ça surchargerait le schéma. On utilise donc le label "terre" à chaque endroit relié à la terre.

Les 2 représentations ci-dessous sont équivalentes (le pointillé rouge n'est pas représenté, il sous-entend le fil):  
![labels](images/efs_002/05_schema_label.png)


## Terre, masse (et potentiel 0V)
[![youtube](images/youtube.png) (00:17:24)](https://www.youtube.com/watch?v=Cx9RoBirwyk&t=17m24s) 
- la terre est un potentiel relié strictement à la terre (par un piquet métallique dans l'installation électrique
)  
![branchement de terre](images/efs_002/06_branchement_terre.png)
- la masse est la carcasse métallique d'un objet branché au secteur, et qui est reliée à la terre pour des raisons de sécurité des personnes.  
![masse](images/efs_002/07_masse.png) 


***************************

# Les résistances
[![youtube](images/youtube.png) (00:25:00)](https://www.youtube.com/watch?v=Cx9RoBirwyk&t=25m00s) 

## Caractéristiques :
- Une résistance est un dipôle (2 pattes)  
- Certains utilisent le terme de "resistor" pour l'objet réel avec ses imperfections et "résistance" pour la résistance théorique parfaite. 
- la résistance a une valeur intrinsèque, qui s'appelle ... la résistance
- l'unité de la résistance est le Ohm (Ω) 
- le kilo-Ohm (kΩ) est souvent noté k par exemple 22k )
- on voit parfois la notation R à la place de Ω pour les résistances ayant une valeur en Ω (par exemple 220R pour 220Ω )
- symbole d'un résistance : le zigouigoui américain, ou le rectangle européen :  
![symboles resistance](images/efs_002/09_resistance_symboles.png)

Le but d'une résistance dans la vie, c'est de chauffer.
Quand un courant la traverse une résistance dégage une puissance en Watt (W) : 
```math
P = Ri^2
```

Une résistance s'oppose au passage du courant. On "rétrécit le passage" et on utilise des matériaux qui empêchent aussi les charges de passer facilement.  
Le ralentissement des charges amène une perte d'énergie de ces charges, d'où une énergie "perdue" qui se dissipe dans le composant.

Parfois la chaleur est le but recherché (radiateur, appareil à raclette), parfois non.

## Démo de la chauffe avec caméra IR : 
[![youtube](images/youtube.png) (00:29:53)](https://www.youtube.com/watch?v=Cx9RoBirwyk&t=29m53s)  

![résistance Infra Rouge](images/efs_002/10_resistance_chaleur.jpg)

## A quoi ressemble une résistance physique
[![youtube](images/youtube.png) (00:33:37)](https://www.youtube.com/watch?v=Cx9RoBirwyk&t=33m37s) 

[![wikipedia](images/wikipedia.png) Les résistances](https://fr.wikipedia.org/wiki/R%C3%A9sistance_(composant))

###  traversantes 
- résistance "classique" 1/4 W traversante  
![résistance](images/efs_002/08_resistance.png)
- résistance plus grosses dissipant plus de puissance

### CMS
![résistance CMS](images/efs_002/11_resistance_cms_recto_verso.jpg)


Pour supporter et dissiper la chaleur (un simple fil fin est plus un fusible qu'une résistance), la résistance est construite autour d'un substrat. En général de la céramique qui supporte bien la chaleur.  
La céramique est un isolant.

On saupoudre donc le substrat avec un matériaux laissant plus ou moins bien passer le courant.  
![schéma substrat_résistance](images/efs_002/12_resistance_substrat.png)  
ça peut être un film de carbone pour les résistances 5% classique.

### Résistance à couches de carbone
Vidéo de l'intérieur d'une résistance à film de carbone  
[![youtube](images/youtube.png) (00:39:21)](https://www.youtube.com/watch?v=Cx9RoBirwyk&t=39m21s)   
![coupe résistance](images/efs_002/13_resistance_interieur.png) 

- le substrat céramique est en blanc
- le film de carbone est la fine pellicule noire autour de la céramique
- une résine protectrice couleur "moutarde" entoure le tout

Le [code couleurs](https://sites.google.com/view/renepaulmages/electronique/) des résistances

### Résistance à film métallique
https://eepower.com/resistor-guide/resistor-materials/metal-film-resistor


## Choisir une résistance
### Relation courant tension d'une résistance
[![youtube](images/youtube.png) (00:56:09)](https://www.youtube.com/watch?v=Cx9RoBirwyk&t=56m9s) 

La résistance possède 
- une valeur (en ohm) donnée pour 25°C
- une tolérance (10%, 5%, 1%,...)
- une puissance maximale dissipée (1/4 W, 0.1 W,...)
- un coefficient thermique (en Ω/°C) :
La valeur d'une résistance change avec la température. En général plus il fait chaud, moins ça résiste. 

[![youtube](images/youtube.png) (01:06:03)](https://www.youtube.com/watch?v=Cx9RoBirwyk&t=1h6m3s)  
La relation entre la tension et le courant, au travers d'une résistance, est linéaire:
```math
i= \frac U R 
\\
ou 
\\
U=Ri
\\
ou 
\\
R=\frac U i
```
avec :
- U : la tension en Volt
- i : l'intensité en Ampère
- R : la résistance en Ω



![diagramme U/I résistance](images/efs_002/14_graph_uri.png) 

**Une résistance ne réduit pas la tension, ni l'intensité, mais impose donc juste qu'on soit sur cette droite.**




### Circuit simple : résistance et led

#### La théorie sur papier 

[![youtube](images/youtube.png) (01:07:40)](https://www.youtube.com/watch?v=Cx9RoBirwyk&t=1h7m40s)  


- un générateur de tension fournissant 5V continu (pile, batterie, alimentation stabilisée,... ).
- un interrupteur
- une résistance
- une diode électro-luminescente (LED ou DEL) dont la caratéristique en fonctionnement (la diode est dans le bon sens et elle brille) sont :
    - 2V
    - 20 mA

![circuit résistance et LED](images/efs_002/15_circuit_resistance_led.png)     

On ferme le l'interrupteur ⇒ des charges commencent à circuler.  
Le courant va du + vers le - (sens conventionnel).  
On note U la tension dans la résistance (convention récepteur :  à l'inverse du courant ⇒ flèche de la tension vers le haut).  
La tension aux bornes de la diode est de 2V (par construction).  
Donc U = 5V - 2V = 3V  
Comme on veut 20 mA traversant la diode, et que le circuit est une boucle simple, l'intensité i sera de 20mA dans tout le circuit.  
Pour que les conditions U=3V et i=20mA soient respectées, étant donné que la relation tension/intensité dans une résistance est linéaire et respecte U=Ri,  on doit donc choisir une résistance de R = U/i = 3/20.10¯³ = 150Ω  

En choisissant une résistance de 150Ω , l'équilibre du circuit se fera bien correctement avec une intensité de 20mA et une tension de 3V aux bornes de la résistance. La LED s'allumera de façon optimale.


La caractéristique courant/tension d'une diode (led ou autre) ressemble à la figure ci-dessous.  
![diagramme couran/tension diode](images/efs_002/16_diagramme_u_i_diode.png)    

Si une tension "de seuil" est atteinte (2V pour notre LED) alors la diode laisse passer le courant quasiment sans limite. (la partie droite du diagramme).  
Dans le cas contraire, notamment si elle est branchée à l'envers et que la tension à ses bornes est négative ( inférieure à  Vseuil),  alors aucun courant ne passe : la diode bloque le courant en inverse.  
_Le fonctionnement des diodes sera décrit en détail dans un prochain épisode._

Un "point de fonctionnement" doit donc être trouvé pour que les 2 caractéristiques des composants (résistance et led) "matchent"

#### Avec une vraie LED (jaune)
[![youtube](images/youtube.png) (01:14:30)](https://www.youtube.com/watch?v=Cx9RoBirwyk&t=1h14m30s)


La diode utilisée :
- diode jaune de marque Kingbright
- achetée chez Digikey (https://www.digikey.fr)
- n° produit et fiche descriptive : [754-1284-ND](https://www.digikey.fr/fr/products/detail/kingbright/WP7113YD/1747683?s=N4IgTCBcDaIOwFYAsBaAjGAHKgdgExAF0BfIA)

Les principaux éléments du composant sont disponibles directement dans la page web, il convient cependant de valider ça en vérifiant dans le [PDF](https://www.kingbrightusa.com/images/catalog/SPEC/WP7113YD.pdf) de la Fiche technique (datasheet) officielle (le lien est donné sur la page)

Pour notre led, les caractéristiques nominales sont 1.95V pour 10mA
![diagramme couran/tension diode](images/efs_002/17_diode_carac_pdf.png) 

La datasheet donne toutes les caractéristiques du composant:
 - utilisations typiques
 - paramètres nominaux
 - paramètres maximaux
 - dimensions
 - longueur d'onde
 - angle d'éclairage
 - diagrammes de caractéristiques ( U/I par exemple)
 - packaging (pour l'utilisation en industrie de manière totalement automatisée par des machines)
 - ...

Avec une alimentation de laboratoire, il est possible de régler la tension qu'on souhaite.

sous 2V : la led brille correctement et s'équilibre à 10mA.  
![led alimentation labo](images/efs_002/18_led_alim_labo.png) 

Si on alimente la diode avec 10V sans limiter le courant, on va dépasser les caractéristiques maximum et la faire exploser.  
On ajoute donc une résistance correctement dimensionnée pour limiter le courant.  
2V au borne de la led, reste donc 10-2=8V au borne de la résistance, soit, pour un courant de 10mA souhaité  R = U/I = 8/0.01 = 800Ω  

![calcul de R](images/efs_002/19_calcul_r.png) 

En pratique on prendra une résistance d'1k

Branchement et test sur breadboard : 
[![youtube](images/youtube.png) (01:32:24)](https://www.youtube.com/watch?v=Cx9RoBirwyk&t=1h32m24s)

Avec la résistance de 1k au lieu des 800Ω calculés, en mesurant la tension de la led au multimètre, on trouve 1.93V  et 8.14V aux bornes de la résistance. C'est tout à fait correct comme point de fonctionnement.

Pour connaitre le sens d'une diode, il y a une pate "plus" longue à brancher sur le (+). Il y a aussi un méplat sur le boitier plastique qui indique le coté (-).

#### Et si on branche la diode à l'envers ?

Si on retourne la diode, elle ne s'allume pas : la diode bloque le passage du courant dans le sens inverse.  
Si on augmente la tension en inverse, on arrivera à la destruction de la diode. Idem si on fait passer trop de courant en sens direct.

la tension aux bornes de la diode, quand elle est branchée en inverse dans notre circuit, sera de 10V. 
On n'aura plus aucun courant qui passe dans le circuit. Donc il y aura 0V aux bornes de la résistance.


## Le pont diviseur 

### définition et formule
[![youtube](images/youtube.png) (01:41:08)](https://www.youtube.com/watch?v=Cx9RoBirwyk&t=1h41m08s)

Un pont diviseur ce sont 2 résistances l'une derrière l'autre(notées R₁ et R₂).  
On dispose d'une tension U aux bornes de l'ensemble des 2 résistances, et on s'intéresse aux tensions aux bornes de chaque résistance (notées U₁ et U₂), notamment U₂.  
On suppose, pour le moment, que le pont diviseur "n'est pas chargé". C'est à dire qu'il n'y a aucun courant qui "sort" entre  les résistances (i=0). Dans ce cas, le courant qui sort du générateur est égal à i₁ qui traverse R₁ et à i₂ qui travers R₂.

![pont diviseur](images/efs_002/20_pont_diviseur.png) 

Comment calculer U₂ à partir de U, R₁ et R₂ :
```math
\begin{aligned}


U_2 &= R_2.i_2 \\

i_2 &= \frac {U_2} {R_2}  \\\\


U_1 &= R_1.i_1 \\ 
U_1 &= R_1.i_2 \\
U_1 &= R_1. \frac {U_2} {R_2}
\\\\


U & = U_1    &+ U_2      \\
U & = R_1 \frac {U_2} {R_2}  &+ U_2  \\

U &= U_2 ( \frac {R_1} {R_2} + 1) \\
U &= U_2 ( \frac {R_1 + R_2} {R_2} ) \\\\
U(\frac {R_2}{R_1 + R_2} ) &= U_2

\end{aligned}
```


A l'entrée on a une tension U, en sortie, on diminue la tension U d'un facteur R₂ / (R₁+R₂).  
Ce facteur étant toujours < 1, la tension de sortie U₂ est toujours plus petite que U.


### Exemple concret : alimenter un arduino avec une batterie de 24V
[![youtube](images/youtube.png) (01:46:13)](https://www.youtube.com/watch?v=Cx9RoBirwyk&t=1h46m13s)

L'idée est d'alimenter un arduino en 5V avec une batterie de voiture de 24V.

Si on branche directement l'arduino sur le 24V, on va construire un magnifique générateur de "magic smoke".

Par contre on peut utiliser un pont diviseur pour abaisser la tension de la batterie de 24V à 5V, et ainsi alimenter l'arduino correctement.

_**Remarque sur l'efficacité énergetique du montage :**  
 Ce montage fonctionne, et c'est la méthode la plus élémentaire pour abaisser une tension. Mais il n'est pas efficace en terme de consommation d'énergie. En effet, tout le courant qui passe dans les résistances du pont est "perdu" pour l'arduino._ 


 Lors du calcul du pont diviseur on avait fait l'hypothèse d'un pont non chargé (i=0). Maintenant l'arduino va consommer du courant, donc i n'est plus nulle.  
 On est dans le cas d'un pont diviseur "chargé". Au fur et à mesure que l'arduino (la charge) va pomper du courant, notre formule calculée ci-dessus ne va plus être juste, et le pont diviseur va dériver de sa tension "à vide" (tension calculée du pont non chargé).  
Une astuce pour éviter de trop dériver est de choisir un courant de pont (qui traverse les résistances), bien supérieur au courant de charge (qui va vers l'arduino).

On considère que si  Ipont > 10 x Icharge, la tension du pont diviseur ne dérivra pas trop.

On choisira donc non seulement un ratio R₁ et R₂ pour que la tension de 24V soit abaissée en 5V, mais aussi des valeurs de R₁ et R₂ pour que le courant les traversant soit "important" par rapport au courant alimentant l'arduino.

C'est le programme du prochain épisode !

### Un pont diviseur peut en cacher un autre

Les ponts diviseurs se cachent dans des circuits apparement simples.

En effet, les générateurs (piles, alim,... ) ne sont jamais parfait et peuvent être vu comme la mise en série d'un générateur parfait et d'une résistance interne...

Mais on verra ça une autre fois.



# Conclusion : fumage de résistances et de leds \o/
## Une résistance, ça fume énormément
[![youtube](images/youtube.png) (01:53:42)](https://www.youtube.com/watch?v=Cx9RoBirwyk&t=1h53m42s)

![magic smoke](images/efs_002/21_magic_smoke.png) 

Il est à noter que quand une résistance, sur un circuit, est noircie, ce n'est pas normal. Elle a fumé et est probablement morte cramée !

## Et paf la led (ou pas)
[![youtube](images/youtube.png) (02:00:05)](https://www.youtube.com/watch?v=Cx9RoBirwyk&t=2h00m05s)

![paf la led](images/efs_002/22_paf_led.png) 

---
![fin](images/thats_all_folks_bugs.png)  
Dans le [prochain épisode](efs_ep003.md) nous verrons en détail le pont diviseur.

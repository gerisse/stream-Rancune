# EFS ep7 : Ondule ton corps ...

**Épisode 7 ( 8 Juillet 2022 )** 

<img src="images/efs_007/01_chat_ondule.png" alt="chat" width="300"/>

 -  ![youtube](images/youtube.png) Youtube
     -  https://www.youtube.com/watch?v=sQg1zhb10AU
 - ![twitch](images/twitch.png) Twitch : 
     - https://www.twitch.tv/videos/1525968657

# Previously in EFS
[![youtube](images/youtube.png) (00:07:44)](https://www.youtube.com/watch?v=sQg1zhb10AU&t=7m44s)  


On a vu dans l'[épisode précédent](efs_ep006.md) que quand on soumettait une tension sinusoïdale à un circuit RC, l'intensité (en fait la tension au borne de la résistance, proportionelle à l'intensité dans le circuit avec $`i=\frac U R`$ ), était en avance par rapport à la tension du condensateur Uc.  
![](images/efs_007/02_previously.png)

Le diagramme Uc/I d'un condensateur ne peut pas être tracé, l'intensité ne variant pas selon Uc, mais selon la variation de Uc (la dérivée de Uc par rapport au temps).

Pour caractériser tout ça il va falloir introduire de nouveaux concepts.

# Fourier et les signaux périodiques
[![youtube](images/youtube.png) (00:12:00)](https://www.youtube.com/watch?v=sQg1zhb10AU&t=12m00s)  

## Définition des signaux périodiques
Quand on a un signal périodique (motif qui se répète régulièrement au bout d'un certain temps), on va définir :
- le temps de répétiton, la durée du motif nommée la **période** notée **T** exprimée en secondes
- Le nombre de répétitions du motif par secondes : la **fréquence** notée **F** ou **f** exprimée en Hertz (Hz) 

Le Hertz est un "nombre de fois par secondes) et on a la relation $`f = \frac 1 T `$

Ceci s'applique à tout signal périodique (triangulairen, carré, sinusoïdal, ou 'Bart Simpson' comme ci-dessous)

 ![](images/efs_007/03_signal_periodique.png)
  
## La transformée de Fourier
Joseph Fourier 
[![wikipedia](images/wikipedia.png)](https://fr.wikipedia.org/wiki/Joseph_Fourier) a vécu pendant la révolution et touchait sa bille en mathématique.  
Il va apporter un théorème important : tout signal périodique peut se décomposer en une somme de sinusoïdes.

La plupart des systèmes électronique qu'on connait ont des  propriétés intéressantes :  
- Quand on met une sinusoïde en entrée, la sinusoïde en sortie peut être décalée dans le temps, plus grande, plus petite, mais elle reste en général à la même fréquence.
- Si on a calculé la réponse du système à une première fréquence, et la réponse du système à une 2ème fréquence ; quand on met la somme des 2 fréquences en entrée, la sortie est bien la somme des 2 fréquences calculées.

Pour étudier la réponse d'un sytème électronique à un signal complexe, il suffit donc de décomposer le signal en sa somme de sinusoïdes, étudier la réponse du système pour chaque sinusoïde, et additionner les réponses pour trouver la forme du signal de sortie.

Note : la transformée de Fourier peut donner une somme infinie de sinusoïdes pour certains signaux...   
De plus pour les signaux non périodiques (comme la voix ou un signal audio), il est quand même possible de découper ça en somme de sinusoïdes.

![](images/efs_007/04_analyse_signal_complexe.png)

Pour voir un exemple de décomposition : [![wikipedia](images/wikipedia.png)](https://fr.wikipedia.org/wiki/S%C3%A9rie_de_Fourier)

# Mesure de Uc et i dans un montage alimenté par une sinusoïde
[![youtube](images/youtube.png) (00:22:01)](https://www.youtube.com/watch?v=sQg1zhb10AU&t=m22s01)  


Le schéma de mesure comporte un condensateur en série avec une résistance (servant à mesurer le courant).  
Le générateur produit une sinusoïde.  
L'oscillo mesurera en voie ① la tension Uc et en voie ② le courant (proportionnel à Ur)

![](images/efs_007/05-schema_etude.png)

Voici le montage "réel"  
![](images/efs_007/06_montage_etude.png)


Et le résultat sur l'oscillo.    
La courbe Jaune est Uc et la courbe bleue Ur=Ri  
![](images/efs_007/07_oscillo_etude.png)




Si on change la fréquence du générateur, on voit que la courbe de la tension Uc diminue en amplitude quand on augmente la fréquence, et augmente en amplitude quand on baisse la fréquence.  
Le condensateur a un comportement qui dépend de la fréquence.

Le courant (en bleu) est bien "en avance" sur la tension comme déjà remarqué précédemment.  
Quand on chnage la fréquence du générateur, ce décalage ne change pas.

On a donc différentes modifications sur le signal. 
- une modification d'amplitude 
- une modification de "décalage" : un déphasage


# Modélisation mathématique
[![youtube](images/youtube.png) (00:32:56)](https://www.youtube.com/watch?v=sQg1zhb10AU&t=32m56s)

## Rappel

![](images/efs_007/08_circuit_math.png)

On a déjà vu plusieurs fois :
```math
\begin{align*}
q &=CU
\\ \  \\
\underbrace{\frac {dq}{dt}} _{i}  &=C \frac {dU_c} {dt}
\\ \  \\
\Big\|\  \ \ \ \  i &= C \frac {dU_c} {dt}
\end{align*}
```

Dans cette écriture, on n'a pas encore introduit la tension sinusoïdale du générateur.

## Mathématiques et (co)sinusoïdes
En math, une sinusoïde s'écrit : $`U = A \cos (\omega t + \phi) `$ ...  
Reprenons depuis le début :
 

## Retour sur la trigonométrie
### Pythagore
Dans un triangle rectangle de cotés de longueurs a et b (au niveau de l'angle droit) et d'hypoténuse de longueur $`\mathcal{H}`$, un certain Pythagore a dit : $`= \mathcal{H}^2 = a^2  + b^2`$  
![](images/efs_007/09_pythagore.png)

### Sinus et cosinus
Dans le même triangle rectangle, on définit :   
- $`\cos = \frac {coté \ adjacent} {hypoténuse} = \frac a {\mathcal{H}} `$
- $`\sin = \frac {coté \ opposé} {hypoténuse} = \frac b {\mathcal{H}} `$

![](images/efs_007/10_cos_sin.png)

### Les unités d'angle :
- degrés  → 0 … 90° … 360°
- radians →  0 … $`\frac \pi 2`$ … $`2\pi`$

Le radian est une unité extrèmement pratique :  
Quand on prend un cercle de rayon R et un angle $`\theta`$ (en radian), l'arc intercepté par cet angle sur le cercle a pour longueur $`\theta R `$  
Un cas particulier est la longueur du cercle complet (pour un angle de $`2\pi`$ radians donc) qui vaut $`2\pi R`$.   
![](images/efs_007/11_cercle_radian.png)

### Le cercle unitaire (cercle trigonométrique)

C'est un cercle de rayon 1 qui est très pratique pour travailler sur les sinus et cosinus.
Il permet de d'avoir $`\mathcal{H} = 1`$ et donc on a directement le cosinus en abcisse et le sinus en ordonnée.

Sur le cercle trigo, on tourne dans le sens inversdes aiguilles d'une montre : le sens trigonométrique.

Pour un angle de 0 :
- cos(0)=1
- sin(0)=0

Pour un angle de $`\frac \pi 2`$ :
- cos($`\frac \pi 2`$)=0
- sin($`\frac \pi 2`$)=1

![](images/efs_007/12_cercle%20unitaire.png)


## Représentation de $`cos(\theta)`$ par rapport a $`\theta`$

![](images/efs_007/13_cosinus.png)

On a ici une représentation de cosinus par rapport à un angle ($`\theta`$).  

### On veut du temps !
Or dans notre expression $`\omega t + \phi`$, $`\omega`$ est une pulsation en rad/s, t un temps (en s) et $`\phi`$ un angle en radian.  
$`\omega t + \phi`$ est donc bien une expression angulaire, mais dont la variable est le temps t.

### Entre -1 et 1 c'est un peu court 
le cosinus varie entre -1 et 1.  Or un signal sinusoïdale peut avoir une amplitude plus (ou moins) importante.  
En multipliant notre cosinus par A, il est possible de faire varier la courbe entre -A et A. Plus de limite !

## Représentation de U(t)  
On représente :
$`U(t) = A \cos (\omega t + \phi) `$

![](images/efs_007/14_u.png)

Avec :   
 - A : l'amplitude du signal
 - $`\omega`$ : la pulsation du signal
 - $`\phi`$ : la phase à l'origine 

### Amplitude
Elle contrôle la hauteur du signal.  
**Attention** : Ne pas confondre l'amplitude A du signal (de 0V à +AV), et la tension crête à crête (de -AV à +AV). Le signal varie donc de + l'amplitude à - l'amplitude.  

En musique, l'amplitude correspond au volume.  

### Pulsation
La pulsation $`\omega`$ est l'angle effectué sur le cercle trigo en 1s par le signal. Son unité est donc le rad/s.  
On a la relation $`\omega = 2 \pi f = \frac {2 \pi} T`$, en rad/s.  
Plus un signal "tourne vite", plus sa fréquence est elevée.  
Le temps mis pour faire un tour (revenir à son point de départ) soit $`2 \pi`$ radians, correspond à la période T =  $`\frac {2 \pi} \omega`$ secondes.  

En musique, la pulsation (ou fréquence, ou période), joue sur la hauteur du son (plus ou moins aigu/grave)

### Phase à l'origine.
$`\phi`$ permet d'avoir un "décalage temporel" de la courbe (décalage gauche-droite). On introduit un déphasage à l'origine, permettant à la courbe de ne pas commencer à U=Amplitude quand t=0 (quand t=0 cos(0) = 1 donc U(0) = A). 

L'unité de ce déphasage est le radian.  
C'est bien un angle qui "déplace" le point sur le cercle trigo au départ, à t=0, introduisant un "déphasage".  

En musique, la phase ne s'entend pas. Elle peut  néanmoins intervenir sur du matériel audio (stéréo) et le cerveau utilise le déphasage entre les signaux reçus par les 2 oreilles pour déterminer d'ou vient le son.

### Animation
![](images/efs_007/15_animation_sinusoide.gif)



## Continuons la modélisation
On a :
```math
\begin{align*}
\begin{cases}  
    U(t) &= A \cos (\omega t + \phi)
    \\ 
    i &= C \frac {dU_c}{dt}
\end{cases}
\end{align*}
```

donc 
```math
\begin{align*}
    i &= C \frac {d}{dt} \Big(A \cos (\omega t + \phi)\Big)  
    \\
    &=CA \frac {d}{dt} \Big(\cos (\omega t + \phi)\Big) 
     \\
    &=-CA\omega \sin (\omega t + \phi) 

\end{align*}
```

Or : $` - \sin(x) = \sin(x + \pi)`$,  
et $`\sin(X) = \cos(X-\frac \pi 2)`$   
donc $`- \sin(x) = cos (x + \pi -\frac \pi 2)`$   
donc $`- \sin(x) = cos (x+\frac \pi 2)`$ 


Au final  

```math
\Big\|\  \ \ \ \  i = C\omega A \cos (\omega t + \phi + \frac \pi 2) 
```

**Finalement :**   
Quand on applique une tension sinusoïdale à un condensateur :
- le courant est aussi de forme sinusoïdale
- La fréquence du courant est la même que celle de la tension ($`\omega`$ est le même pour Uc et i)
- Le déphasage entre le courant et la tension est toujours de $`\frac \pi 2`$.  
  Le courant est "en avance" sur la tension.  
  Ça ne dépend pas de la valeur du condensateur, ni de la fréquence.
- L'amplitude du courant ($`C\omega A`$) dépend du condensateur et de la fréquence.



# Conclusion
[![youtube](images/youtube.png) (01:29:09)](https://www.youtube.com/watch?v=sQg1zhb10AU&t=1h29m9s)

- Un signal périodique quelconque se décompose en somme de sinusoïdes
- Un signal sinusoïdale est caractérisé par [$`\omega`$, $`\phi`$, A] : 
    - $`\omega`$ : sa pulsation (ou sa fréquence $`\omega =  2\pi f`$)
    - sa phase à l'origine $`\phi`$
    - son amplitide A
- Un signal, en tension, d'une certaine fréquence génère une intensité d'une même fréquence. (l'amplitude et la phase peuvent varier)

La caractérisation de la transformation des signaux n'a donc pas besoin de $`\omega`$ qui reste identique entre U et i.  
On a donc juste besoin d'un couple [A, $`\phi`$] : une tension et un angle,  qui seront modifiés.

En mathématique, ce qui a une grandeur et un angle peut être représenté par des nombres complexes.

![](images/efs_007/16_conclusion.png)

Ça nous permettra d'amener la notion d'impédance complexe, une sorte de résistance complexe, qui nous permettra d'appliquer la loi d'Ohm sur des nombres complexes.

Pour notre condensateur,  si on fait le rapport entre le courant et la tension, on voit apparaitre un "C $`\omega`$" qui ressemble à une sorte de "résistance" qui varierait avec la fréquence.

On parlera de l'impédance du condensateur $`Z_c = \frac 1 {jC\omega}`$

La suite au prochain épisode...

----- 

![fin](images/thats_all_folks_bugs.png)

Dans le [prochain épisode](efs_ep008.md) parlera d'impédance et de nombres complexes, pour réécrire ce qu'on a vu dans une écriture plus pratique.



# EFS ep11 : Circuits RC & RL - Filtre Passe-bas

**Épisode 11 ( 5 août 2022 )** 

<img src="images/efs_011/01_chat_cut.png" alt="chat" width="300"/>

 -  ![youtube](images/youtube.png) Youtube
     -  https://www.youtube.com/watch?v=vxBHOz1FfJY
 - ![twitch](images/twitch.png) Twitch : 
     - https://www.twitch.tv/videos/1553111931

# Préambule : templates latex de feuilles "scientifiques"
[![youtube](images/youtube.png) (00:04:29)](https://www.youtube.com/watch?v=vxBHOz1FfJY&t=4m29s)

Repository gitlab : 
https://gitlab.com/b5216/blue_papers


- [papier millimétré](https://gitlab.com/b5216/blue_papers/-/blob/main/pdf/A4_millimetre.pdf)
- [papier semilog](https://gitlab.com/b5216/blue_papers/-/blob/main/pdf/A4_semilog_coarse.pdf)
- [papier semilog paysage](https://gitlab.com/b5216/blue_papers/-/blob/main/pdf/A4_semilog_coarse_landscape.pdf)
- [Abaque de smith](https://gitlab.com/b5216/blue_papers/-/blob/main/pdf/A4_smith.pdf)
- ...

# Principe d'une étude fréquentielle de filtres
[![youtube](images/youtube.png) (00:10:25)](https://www.youtube.com/watch?v=vxBHOz1FfJY&t=10m25s)  

Prenons l'exemple d'un filtre RC passe bas.  
L'idée est de construire un pont diviseur, en s'arrangeant pour que les hautes fréquences soient éliminées.  

Rappel : Le condensateur est un fil pour les hautes fréquences et un circuit ouvert pour les basses fréquences.

On va donc ramener les hautes fréquences à la masse avec un condensateur "en bas" dans notre pont diviseur.

![](images/efs_011/02_rc_passe_bas.png)  

## En abscisse : les fréquences
Si on souhaite étudier la réponse d'un filtre en fréquence, on souhaite avoir des informations sur son fonctionnement par exemple entre 0 Hz et quelques GHz.  
Cet écart est énorme.  
Une représentation linéaire 1, 2, 3 Hz jusqu'à 2Ghz ne tiendra pas dans une feuille.  

On triche donc sur l'échelle.  
On défini des tranches "égales" dont l'échelle est multipliée par 10 à chaque tranche :  
10Hz → 100Hz → 1000Hz → 10kHz → 100kHz → 1MHz → 10MHz → 100MHz → 1GHz

Avec cette astuce d'échelle de fréquence modifiée, on peut tracer la représentation fréquentielle d'un filtre, soit 2 graphs :
- l'un indiquant la modification d'amplitude du signal entre l'entrée et la sortie, selon la fréquence.
- l'autre indiquant la différence de phase entre l'entrée et la sortie selon la fréquence.

![](images/efs_011/03_gain_phase_diagrammes.png)

Le papier semi-logarithmique présenté en préambule aura donc tout son intérêt pour tracer ce genre de graphs


Pour étudier un signal complexe, il suffira de le décomposer en sinusoïdes simples (par transformée de Fourier).  Chaque sinusoides sera "passée au filtre" et on additionnera les résultats en sortie pour avoir le signal résultant.


## En ordonnée : gain et déphasage
[![youtube](images/youtube.png) (00:30:17)](https://www.youtube.com/watch?v=vxBHOz1FfJY&t=30m17s) 

###  Graph du gain
En général plusieurs filtres vont se succéder dans un cicuit, la sortie de l'un étant l'entrée de l'autre.  
Pour facilement "cascader" les études, on n'affichera pas la sortie d'un filtre en ordonnée du graph, mais le rapport entre sa sortie et son entrée : $`\frac {U_S} {U_E}`$  
Il suffira donc de multiplier les ratios des différents étages pour trouver le rendement final.

![](images/efs_011/04_chaine_filtre.png)

- Un rendement > 1 indiquera une amplification
- Un rendement < 1 indiquera une atténuation>

Ce rendement n'a pas d'unité (on divise une grandeur par une autre de même unité). 

En ce qui concerne les valeurs possibles, on peut avoir de très faibles atténuations dans certaines fréquences, cottoyant une  amplifications par 10 ou par 100 à d'autres fréquences par exemple.
On se retrouve donc dans un cas similaire aux fréquences, mais qui sera géré de manière différente


### graph de phase 
Pour le déphasage, pas de problème d'échelle en ordonnée : la valeur du déphasage étant un angle, il sera toujours entre $`-\pi`$ et $`\pi`$ (modulo $`2\pi`$)


### cohérences des échelles en ordonnées 

On a vu que l'ordonnée du graph de déphasage peut être "standard" (linéaire) car les valeurs sont contenues dans un interval faible.  
On souhaitera donc ne pas utiliser d'échelle logarithmique pour les ordonnées du graph de gain, afin de rester cohérent avec l'échelle des phases.

L'astuce va être d'utiliser une unité bien choisie en appliquant une petit traitement mathématique au rendement $`\frac {U_S} {U_E}`$, et d'afficher en ordonnée le résultat de ce calcul.  
Cette unité est le **Décibel (dB)**


## le Décibel (dB)
[![youtube](images/youtube.png) (00:30:36)](https://www.youtube.com/watch?v=vxBHOz1FfJY&t=38m36s)

La fonction "qui va bien" pour notre mise à l'échelle est le logarithme :  
![](images/efs_011/05_logarithme.png)

Quand on va vers l'infini, les courbes des logarithmes deviennent très plate.  
On a donc très peu de changement, en ordonnée, pour les très grande valeur de x.  C'est très intéressant pour notre problématique qui est "d'écraser" les grandes valeurs.
Les modifications entre 1 et 10 auront ainsi une granularité plus fines que celle entre 1G et 10G 

Une autre propriété des logarithmes est que $`\ln (a \times b) = \ln(a)+ \ln(b)`$   
Et que donc $`\ln(a^n) = n\ln(a)`$

Pour le décibel on n'a pas choisi le logarithme népérien ln (ou logarithme naturel, ou logarithme en base e), mais le logarithme en base 10 (log ou log₁₀) tel que log(x) = ln(x)/ln(10).
- log(10) = 1
- log(1) = 0 (comme pour tous les logarithmes)

Le décibel est une unité de rapport de **puissances** ayant pour définition :  

$` \Big\| \ \ \ (\frac {P_S}{P_E})_{dB} = 10 log_{10} (\frac {P_S} {P_E}) `$ 

Une unité associée chère au radioamateurs est le dBm, qui est le rapport de puissance par rapport à une puissance "d'entrée" de 1mW:
$`({P_S})_{dBm} = 10 log_{10} (\frac {P_S} {10^{-3}})`$


Mais que ce passe-t-il si on n'a pas un rapport de puissance, mais de tension par exemple.

La tension ou l'intensité sont des grandeurs dites de "champs" qui sont reliées à la puissance par un carré. Par exemple la puissance dissipée par une résistance est P = UI = U**²**/R = Ri**²**
on va donc avoir :  
$`(\frac {P_S}{P_E})_{dB} = 10 log_{10} (\frac {\frac {U_S^2}{R}} {\frac {U_E^2}{R}})  \\ \ \\
(\frac {P_S}{P_E})_{dB} = 10 log_{10} (\frac {U_S} {U_E})^2 \\ \  \\
(\frac {P_S}{P_E})_{dB} = 10  \times 2 log_{10} (\frac {U_S} {U_E})
\\ \ \\
\Big\| \ \ \ 
(\frac {P_S}{P_E})_{dB} = 20 log_{10} (\frac {U_S} {U_E})
`$


Attention à bien choisir la bonne formule selon qu'on a un rapport de puissance ou de tension.

Le niveau 0dB correspond à un ratio Us/Ue de 1.  
- au dessus de 0dB on amplifie le signal
- en dessous de 0 dB on atténue le signal
- Augmenter de 1dB, c'est multiplier la puisance par 10, 

_**Question sur le son** : En son, les "décibels" sont bien aussi un rapport de puissance "sonore". De plus l'oreille humaine n'a pas une sensibilité linéaire mais logarithmique aussi.  
La sensation de passer d'un son de 10W à 100W est la même que de passer de 100W à 1000W_


# Simulation de filtres RC avec NGSpice
[![youtube](images/youtube.png) (01:01:49)](https://www.youtube.com/watch?v=vxBHOz1FfJY&t=1h01m49s)  

![](images/efs_011/06_filtres_RC.png)


## Circuit RC (passe bas)
Notons (cf. en rouge sur le schéma ci-dessous):
- in : le net correspondnat à Ue la tension d'entrée
- out : le net correspondant à la tension de sortie (Uc)
- 0 : le net lié à la terre (la ref des tensions)

![](images/efs_011/07_nets_RC_passebas.png)


```rc.cir
.title circuit RC
* filename rc.cir
* simulation passe bas RC efs ép. 11

R1 in out 10k
C1 out 0 1u
V1 in 0 dc 0 ac 1 PULSE (0 5 1u 1u 1 1)

.control
ac dec 10 1 100k
plot vdb(out)
plot ph(out)
.endc

.end
```

![](images/efs_011/08_simu_spice_rc_passe_bas.png)

On est bien en dessous de 0, pas d'amplification), les basses fréquences "passent" (0db), et les hautes fréquences sont atténuées (pente descendante)


## Circuit CR (passe haut)

On inverse R et C dans la simulation précédente 
```cr.cir
.title circuit CR
* filename cr.cir
* simulation passe haut CR efs ép. 11

R1 out 0 10k
C1 in out 1u
V1 in 0 dc 0 ac 1 PULSE (0 5 1u 1u 1 1)

.control
ac dec 10 1 100k
plot vdb(out)
plot ph(out)
.endc

.end
```
![](images/efs_011/09_simu_spice_cr_passe_haut.png)


## Fréquence de coupure
[![youtube](images/youtube.png) (01:32:27)](https://www.youtube.com/watch?v=vxBHOz1FfJY&t=1h32m27s) 

A quelle fréquence considère t-on qu'un filtre "laisse passer" ou  "ne laisse pas passer". Quelle atténuation du signal considère t-on comme "suffisante" pour dire qu'en deça, le signal est considéré comme "coupé".

Les physiciens ont définis une telle "fréquence de coupure". 

La limite a été définie à -3dB. Quand le signal est atténué de -3dB, la fréquence correspondante est définie comme "fréquence de coupure" du filtre.

Pourquoi -3db, parce que ça correspond à une puissance transmise de 50% : 

```math
\begin{align*}
10 \log (\frac{P_S}{P_E})=  -3  
& \Leftrightarrow \frac {\ln(\frac {P_S} {P_E})} {\ln(10)} = - \frac 3 {10} 
\\
& \Leftrightarrow \ln(\frac {P_S} {P_E}) = - \frac 3 {10} \ln(10)
\\
&  \Leftrightarrow \frac{P_S}{P_E} = e ^{-\frac 3 {10} \ln(10)}
\\
& \Leftrightarrow \frac{P_S}{P_E} = 0.5 = 50 \%
\end{align*}
```

En tension, si on refait le même calcul (-3/10 devient -3/20), ça correspond à 70%


Pour un passe bas, ce qui est inférieur à la fréquence de coupure est considéré comme "transmis" au delà, on considère les fréquences comme "coupées"

Pour un passe haut, ce sont les fréquences supérieures à la fréquence de coupure qui sont "passées"

Un passe bande à 2 fréquences de coupure, une basse et une haute, et laisse passer la bande entre ces 2 fréquences.

La pulsation de coupure d'un filtre RC : $`\omega_C = \frac 1 {RC} = 2 \pi f_C `$

La fréquence de coupure d'un filtre RC : $`f_C=\frac 1 {2\pi RC}`$



Astuce graphique pour déterminer la pulsation de coupure, la tangeante de la "pente" de la courbe coupe la ligne des 0dB exactement à la fréquence de coupure.

![](images/efs_011/10_graph_freq_coupure.png)


# Simulation de filtres RL (exercice hors vidéo)

## Circuit RL
```rl.cir
.title circuit RL
* filename rl.cir
* simulation passe haut RL efs ép. 11 (exercice)

R1 in out 220
L1 out 0 100m
V1 in 0 dc 0 ac 1 PULSE (0 5 1u 1u 1 1)

.control
ac dec 10 1 100k
plot vdb(out)
plot ph(out)
.endc

.end
```

- Caractéristique de passe haut.
- Fréquence de coupure de $`fc=\frac 1 {2\pi} \frac {R}{L}=\frac {220} {2 \pi 100.10^{-3}} \approx 350 Hz`$
- Déphasage passe de $`\frac \pi 2`$ à 0

![](images/efs_011/11_circuit_RL_passe_haut.png)


## Circuit LR
```lr.cir
.title circuit LR
* filename lr.cir
* simulation passe bas LR efs ép. 11 (exercice)

R1 out 0 220
L1 in out 100m
V1 in 0 dc 0 ac 1 PULSE (0 5 1u 1u 1 1)

.control
ac dec 10 1 100k
plot vdb(out)
plot ph(out)
.endc

.end
```

- Caractéristique de passe bas.
- Fréquence de coupure de $`fc=\frac 1 {2\pi} \frac {R}{L}=\frac {220} {2 \pi 100.10^{-3}} \approx 350 Hz`$
- Déphasage passe de 0 à $`-\frac \pi 2`$   
![](images/efs_011/12_circuit_RL_passe_bas.png)
----

![fin](images/thats_all_folks_bugs.png)

Pause estivale, Le [prochain épisode](efs_ep012.md) sera fin août. On attaquera le RLC, circuit qui nous amènera des surprises !











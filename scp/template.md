# Soirée Chill et Projets ep : 

**Épisode 13 ( 29 septembre 2022 )** 

----
![](images/under_construction.jpg)  

---- 

<img src="images/scp_013/01_chat.png" alt="chat" width="300"/>

 -  ![youtube](images/youtube.png) Youtube
     -  https://www.youtube.com/watch?v=
 - ![twitch](images/twitch.png) Twitch : 
     - https://www.twitch.tv/videos/1604640917
     - https://www.twitch.tv/videos/1604674037
     - https://www.twitch.tv/videos/1604765870

# Préambule

Dans [l'épisode précédent](scp_ep012.md) nous avions vu la chaîne de compilation et les logiciels necessaires pour compiler des programmes pour STM32 (blue pill), ainsi que pour dialoguer avec la carte via openOCD.

Nouas allons maintenant compiler notre premier programme, le faire fonctionner, et expliquer l'usage des différents fichiers générés ainsi que du code.

# Projet template blue pill

repo git : https://github.com/Rancunefr/template_bluepill

## structure 
- `app` : répertoire contenant notre application
    - `include` 
    - `src` : le répertoire de sources,  Le gros du programme se trouvera dans `main.c`.  
- `cmsis` :  
  - `core` : contient les librairies (.h) fournies par les constructeurs du cœur ARM. 
  - `device` : includes et .c propres a notre stm32 particulier. Ainsi que le petit code assembleur de démarrage de la carte (.s). 
- `docs` : les 3 pdfs de documentation vues dans l'épisode précédent (schéma, datasheet, manuel de ref.)
- `openocd` : le fichier de conf pour lancer openOCD avec avec ke ST-LINK V2 et la  blue pill. cette config utilise des fichiers de conf openOCD venant avec le logiciel. 
- `Makefile` : venant de cubeMX et mis au propre, pour le lancement des la compilation, etc.  
  - Il contient la définition du préfixe du compilateur à utiliser ("arm-none-eabi-"  par défaut).
  - ligne 6-13 : les fichiers source du projet : 4 fichiers ".c", 1 ".s"
    - main.c notre programme
    - fichier .it pour les interruptions
    - séparation de bsp (Board Specific Part) le code propre à la carte (allume la led), du code du micro-contrôleur (bascule PC13)


te de la blue pill clignote rapidement): 
```
gef➤  monitor reset run
gef➤ 
```


##  Explication de code

[A suivre...]  : https://www.twitch.tv/videos/1604674037?t=29m
----

![fin](images/thats_all_folks_porky.png)











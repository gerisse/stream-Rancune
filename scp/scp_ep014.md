# Soirée Chill et Projets  : SMT32 GPIO(2) & MMIO

**Épisode 14** 

----
## But :

Simplifier les fichiers fournis par stm32cubemx : 

Se passer du fichier stm32f1xx.h (proprre à ST, mais on souhaite s'en passer : la libc et le compilo suffisent)

 -  ![youtube](images/youtube.png) Youtube
     -  https://www.youtube.com/watch?v=hZUb9BGy-tw&list=PL1FnGqCKiFzHUhGsfUfgYgUbP6cnldX31&index=29


## Contenu :

Les modes de Boot :
	BOOT0
	BOOT1

Simplification du template de projet dans les episodes précédents. cf https://github.com/Rancunefr/template_bluepill


Dans le dossier *docs* de ce repo, Les 2 docs necessaires : 

	-le reference manual  (RM0008 ici)
	-le datasheet du proceseur 


2 schéma ipts :

	- celui d'un GPIO, page 160 de RM0008

[![https://imgur.com/hxmUmqC.png](https://imgur.com/hxmUmqC.png)](https://imgur.com/hxmUmqC.png)

	- le mapping memoire, page 34 de datasheet :

[![https://imgur.com/S6uNpQq.png](https://imgur.com/S6uNpQq.png)](https://imgur.com/S6uNpQq.png)

Comment s'architecturent les 2, GPIO et mémoire ?

on le comprend avec le schéma :

[![https://imgur.com/zM8tL0x.png](https://imgur.com/zM8tL0x.png)](https://imgur.com/zM8tL0x.png)


Parametrage de GPIO Port C : 

 - pour la clock, via APB2ENR :

		RCC->APB2ENR |= RCC_APB2ENR_IOPCEN

 - pour le mode , via CRH :

		GPIOC->CRH |= GPIO_CRH_MODE13_Msk ;
   		GPIOC->CRH &= ~GPIO_CRH_CNF13_Msk ;

Recherchons les via la cli :

	grep -R GPIOC*

  pour trouver la définition de GPIOC

C'est le début d'une séqunece de recherche dans les #define , pour arriver à ces infos :

[![https://imgur.com/0SF0O9o.png](https://imgur.com/0SF0O9o.png)](https://imgur.com/0SF0O9o.png)

résumé à :

[![https://imgur.com/11eBqED.png](https://imgur.com/11eBqED.png)](https://imgur.com/11eBqED.png)

Bon artcile : https://a3f.at/articles/register-syntax-sugar pour voir , comment en c, faire de differentes facons , une operation en memoire
# Soirée Chill et Projets ep012 : Premiers pas sur STM32

**Épisode 12 ( 22 septembre 2022 )** 


<img src="images/scp_012/01_chat.png" alt="chat" width="300"/>

 -  ![youtube](images/youtube.png) Youtube
     -  https://www.youtube.com/watch?v=jz3Qr463W6o
 - ![twitch](images/twitch.png) Twitch : 
     - https://www.twitch.tv/videos/1598207449

# Préambule

Le but de ce live est de découvrir les puces STM32 [![wikipedia](images/wikipedia.png)](https://fr.wikipedia.org/wiki/STM32) (microcontrolleurs ARM développés par STMicroelectronics [![wikipedia](images/wikipedia.png)](https://fr.wikipedia.org/wiki/STMicroelectronics) ) .  
Nous verrons la mise en place d'un environnement pour un  projet simple, le "hello world" du monde électronique : faire clignoter une led.  
- explications des grands principes de fonctionnement de la puce et des cartes blue pill
- les logiciels (libres)  nécessaires pour programmer, débugguer, compiler, déployer des binaires sur la carte.
- Utilisation de cubeMX (outil 'lourd' de ST) pour générer un squelette de projet le plus minimaliste possible.
- un squelette (template) simple de projet blue pill


L'idée est de partir d'un projet "autogénéré" par les outils ST pour le décortiquer et voir comment tout fonctionne et ce qui est nécessaire. 
Il sera possible par la suite d'utiliser ce squelette pour démarrer des projets sans outils "lourds".

A terme, utilisation d'outils libres génériques vim, gcc, gdb, openocd,...

Le hardware utilisé est une "blue pill", carte bon marché à base de STM32F103  [![wikipedia](images/wikipedia.png)](https://fr.wikipedia.org/wiki/STM32#Série_F1)   et d'un programmeur ST-Link V2 "générique" coûtant quelques euros. 

Ci dessous les composants (le site marchand est pour l'exemple, il est possible de trouver moins cher) :
- blue pill : https://www.az-delivery.de/en/products/stm32f103c8t6
- programmeur ST-link V2 : https://www.az-delivery.de/en/products/st-link-v2-mini-simulator-download-programmer-stm8-6-stm32


## Les différentes cartes STM32.
Pour découvrir et tester la programmation sur STM32, il existe plusieurs types de cartes.

### Les cartes STMicroelectronics "Nucleo"
Cartes officielles de ST, les cartes "Nucleo"  permettent directement et facilement de tester et programmer des Puces STM32, et comportent quelques périphériques utiles ainsi qu'un programmeur USB "intégré" (mais découpable et utilisable pour programmer d'autres cartes). 

C'est un peu la rolls des cartes de tests. Elles sont directement connues et paramétrées dans les outils de programmation de ST.

Ci-dessous une carte nucléo avec un STM32F4 (moyenne gamme, haute performance)  ![wikipedia](images/wikipedia.png)](https://fr.wikipedia.org/wiki/STM32#S%C3%A9rie_F4), son programmeur découpable en haut, ses broches compatibles arduino (Attention à la logique qui reste en 3.3V pour les STM32, mêmes si certaines broches sont "résistantes" au 5V)

![](images/scp_012/02_nucleo_F4.png)


Ci-dessous Une carte nucleo plus "grosse" avec une gamme plus évoluée : le STM32F7, qui est livrée avec un connecteur rj45, permettant de jouer avec une pile TCP/IP, et le réseau.

![](images/scp_012/03_nucleo_F7.png)

Pour les étudiants, il est sans doute possible de demander des cartes nucléo à ST gratuitement ou presque. (c'était possible en tout cas avant la pénurie électronique post-covid).



### Le kit "blue Pill"

Beaucoup moins cher et plus "minimaliste".  
La carte blue pill contient :
- 2 quartz externes (32.768Khz et 8Mhz)
- 2 leds (un témoin d'alimentation, et l'autre programmable sur un GPIO)
- Une interface de programmation (les 4 broches d'où partent les fils sur la photo ci-dessous)
- Une prise micro-USB pour l'alimentation ou pour utiliser comme périphérique USB
- Un bouton reset
- Quelques pins de configurations du type de boot (jumpers)

Le programmeur  ST-LINK V2 (en bas/droite sur la photo ci-dessous) qui permet, en le reliant à la carte STM32 par 4 fils (connecteur en bas), et à l'ordinateur par USB (sous le bouchon en haut), de piloter / interroger /uploader / lire / écrire / vider /... les programmes sur la carte.  
Le programmeur lui même est un STM32 avec un programme kivabien.

![](images/scp_012/04_bluepill.png)


# Programmation de STM32

La programmation des STM32 se fait par un protocole spécifique aux puces ARM (Serial Wire Debug) [![wikipedia](images/wikipedia.png)](https://en.wikipedia.org/wiki/JTAG#Similar_interface_standards) similaire au JTAG,  sur 2 broches : un signal d'horloge SWCLK, et un signal de données SWDIO.  
Il est bien entendu nécessaire d'avoir en plus de 2 fils d'alimentation 3.3V et GND.

Ce protocole est bien documenté et les outils libres comme openOCD le "parlent" sans problème.

## Brochage du programmeur 

![](images/scp_012/05_brochage_programmeur.png)

## Documentation de la carte blue pill (STM32F103C8T6)

  - schéma   
    - [pdf](https://cdn.shopify.com/s/files/1/1509/1638/files/STM32F103C8T6_Mikrocontroller_Schematics.pdf) sur le site az-delivery  
    - [pdf](images/scp_012/STM32F103C8T6_Microcontroller_datasheet_az-delivery.pdf) en copie local
  - datasheet 
    - [pdf](https://cdn.shopify.com/s/files/1/1509/1638/files/STM32F103C8T6_Microcontroller_Datenblatt_AZ-Delivery_Vertriebs_GmbH.pdf) sur le site az-delivery 
    - [pdf](images/scp_012/STM32F103C8T6_Microcontroller_Schematics_az-delivery.pdf) en copie local
  - manuel de référence ST sur les STM32F1x
    - chercher la réf. [RM0008](https://www.st.com/content/st_com/en/search.html#q=rm0008-t=resources-page=1) sur le site https://www.st.com  
    - [PDF version 21 (Février 2021)](images/scp_012/STM32F1x_REF_MANUAL_RM0008_v21_202102) en copie local.


_Une remarque de neteagle2K, sur le chat twitch, indique que la norme USB préconise 1.5k pour la résistance de pull-up de la broche D+ (R10). Le schéma indique une valeur de 4.7k, mais ma carte achetée chez AZ-delivery (via amazon) possède bien une R10 de 1.5k..._  


## Dialoguer avec la blue pill via le programmeur ST-link V2

En reliant les broches du programmeur aux 4 broches de programmation de la blue pill :
- 3.3V
- GND 
- SWDCLK (CLocK) 
- SWDIO (Data Input Output)

Puis en branchant le programmeur à une prise USB de l'ordinateur (Attention la blue pill ne doit pas être elle même branchée en USB), la led rouge de la blue pill s'allume, son éventuel programme se lance, et l'ordinateur est en capacité de "parler" avec la carte stm32 via un logiciel comme openOCD (ou ST-link, outil ST disponible aussi dans toutes les bonnes distrib linux).


### Logiciels nécessaires

#### ⇒ **openOCD** (Open On Chip Debugger) 

 https://openocd.org/

Permet d'envoyer des commandes (raz mémoire, upload...) via le ST-LINK à la puce STM32

Sous Linux Debian-like, quand on installe openOCD avec apt, les fichiers de config se trouvent dans `/usr/share/openocd/scripts/target`

Quand on installe openOCD par compilation (par exemple avec le repository git du raspberry pico https://github.com/raspberrypi/openocd )
les fichiers de config sont installés dans `/usr/local/share/openocd/scripts/target/`


Dans les 2 cas, le fichier de config ci-dessous semble savoir trouver ces fichiers pour les "sourcer"

_fichier `stlinkv2_mini.cfg` :_ 
```
source [find interface/stlink-v2.cfg]
transport select hla_swd
source [find target/stm32f1x.cfg]
reset_config none separate
```

La commande pour lancer openOCD et le connecter au ST-link (branché en USB) :

```
openocd -f stlinkv2_mini.cfg -c "init"
```

**Les cartes STM32 "bizarres"**

Il existe un bon nombre de clones (contrefaçons ?) des puces STM32, notamment sur les blue pills
Certaines cartes ne contiennent pas une vrai puce de chez ST, ça peut être indiqué dessu, ou pas.

Bien qu'ayant acheté mes blues pills sur amazon viz az-delivery, il est possible que ma puce "SSTM32" soient.... différente.
Rien ne l'indique sur la puce, elle ressemble traits pour traits aux puces officielles (certaines contrefaçon se repère a de petite différences visibles sur la puce, ici ce n'est pas le cas) : cf. https://hackaday.com/2020/10/22/stm32-clones-the-good-the-bad-and-the-ugly/  par exemple.

Pourtant par défaut, la commande ci-dessous m'indique une erreur d'idcode :
```
Warn : UNEXPECTED idcode: 0x2ba01477
Error: expected 1 of 1: 0x1ba01477
```
sortie complète :

```
Open On-Chip Debugger 0.11.0-g228ede4 (2022-09-26-00:59)
Licensed under GNU GPL v2
For bug reports, read
        http://openocd.org/doc/doxygen/bugs.html
WARNING: interface/stlink-v2.cfg is deprecated, please switch to interface/stlink.cfg
Info : The selected transport took over low-level target control. The results might differ compared to plain JTAG/SWD
none separate

Info : clock speed 1000 kHz
Info : STLINK V2J29S7 (API v2) VID:PID 0483:3748
Info : Target voltage: 3.236098
Warn : UNEXPECTED idcode: 0x2ba01477
Error: expected 1 of 1: 0x1ba01477

```

**Pour corriger le problème :**  
<strike>il a suffit de modifier le  fichier de config de openOCD concernant les stm32f1x : `/usr/(local/)share/openocd/target/stm32f1x.cfg`.     
Et d'y changer changer l'adresse `0x1ba01477`  par `0x2ba01477` là ou elle est définie.  
On rappelle que ce fichier de config openocd est "sourcé" dans `stlinkv2_mini.cfg` utilisé pour initialiser openOCD dans notre cas.</strike>

Une meilleure méthode est d'ajouter,en haut du fichier `stlinkv2_mini.cfg`, la ligne :
```
## pour certaines blue pill, en commentaire pour une blue pill officielle
set CPUTAPID 0x2ba01477
```
Avec l'avantage de ne pas avoir à modifier de fichiers de config wui viennet avec openocd, mais juste un fichier de config "perso".  
_Merci à bsdsx pour l'astuce : http://blog.bsdsx.fr/2022/10/2022-10-09-stm32-registre.md.html_


La correction permet à openOCD de se lancer correctement :

```
╰─ openocd -f ./stlinkv2_mini.cfg  -c "init"
Open On-Chip Debugger 0.11.0-g228ede4 (2022-09-26-00:59)
Licensed under GNU GPL v2
For bug reports, read
        http://openocd.org/doc/doxygen/bugs.html
Info : The selected transport took over low-level target control. The results might differ compared to plain JTAG/SWD
none separate

Info : clock speed 1000 kHz
Info : STLINK V2J29S7 (API v2) VID:PID 0483:3748
Info : Target voltage: 3.238247
Info : stm32f1x.cpu: hardware has 6 breakpoints, 4 watchpoints
Info : starting gdb server for stm32f1x.cpu on 3333
Info : Listening on port 3333 for gdb connections
Info : Listening on port 6666 for tcl connections
Info : Listening on port 4444 for telnet connections

```

(Il est fort possible que cette carte finisse, à un moment, par poser problème si ce n'est pas une originale. Mais pour cette épisode ça fonctionnera jusqu'au bout.

---


#### ⇒ **arm-none-eabi-gdb**


GDB est les célèbre débugger GNU utilisé en programmation C,  ici, dans une version adaptée à l'environnement ARM.


_**Note** :arm-none-eabi-gdb et les distribution debian-like_  
 _`` arm-none-eabi-gdb`` n'est pas installé avec les package gcc-arm-none-eabi...
 Ce paquet semble inconnu au bataillon sur les debian-like récente. Après recherche il aurait été remplacé par `gdb-multiarch` qui gère le débuggage cross-environnement._


L'installation se fait  par `apt install gdb-multiarch`. 'gef', plugin de gdb, était déjà installé sur mon gdb "standard", ou bien vient automatiquement avec gdb-multiarch.  
Pour rétro-compatibilité avec le nom "standard", il peut être intéressant de faire un lien symbolique ou un alias pour pouvoir appeler "gdb-multiarch" par "arm-none-eabi-gdb"
```
sudo ln -s /usr/bin/gdb-multiarch /usr/bin/arm-none-eabi-gdb
```

- connecter le ST-link à la carte STM32, puis au PC en USB
- lancer openOCD (cf. ci-dessus) pour avoir le logiciel écoutant sur le port 3333 (le ST-link se met à clignoter rouge-bleu)
- dans une autre console, taper `arm-none-eabi-gdb` pour lancer gdb.
- connecter le debugger à openOCD par `target extended-remote localhost:3333`  (ou `target remote localhost:3333` qui semble dépréciée)

la sortie ressemblera donc à ci-dessous, avec des warnings :    
```
╰─ arm-none-eabi-gdb              
GNU gdb (Ubuntu 12.0.90-0ubuntu1) 12.0.90
Copyright (C) 2022 Free Software Foundation, Inc.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
Type "show copying" and "show warranty" for details.
This GDB was configured as "x86_64-linux-gnu".
Type "show configuration" for configuration details.
For bug reporting instructions, please see:
<https://www.gnu.org/software/gdb/bugs/>.
Find the GDB manual and other documentation resources online at:
    <http://www.gnu.org/software/gdb/documentation/>.

For help, type "help".
Type "apropos word" to search for commands related to "word".
GEF for linux ready, type `gef' to start, `gef config' to configure
96 commands loaded for GDB 12.0.90 using Python engine 3.10
gef➤  target extended-remote localhost:3333
Remote debugging using localhost:3333
warning: No executable has been specified and target does not support
determining executable automatically.  Try using the "file" command.
0xfffffffe in ?? ()
[ Legend: Modified register | Code | Heap | Stack | String ]
──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────── registers ────
[!] Command 'context' failed to execute properly, reason: 'NoneType' object has no attribute 'all_registers'
gef➤  

```

Après avoir passé du temps à essayer de comprendre les warnings (qui n'étaient pas présent sur le live de Rancune), il se trouve que gdb attend un fichier exécutable compilé pour STM32, à lire/analyser en premier paramètre. Ici on ne lui en a pas donné, vu qu'on n'en a pas encore compilé un seul.   
Je pensais à un point bloquant pour la suite, mais les commandes de connexions au ST-LINK fonctionnent très bien malgré tout.

![](images/head_slap.gif)




Les commandes gdb commençant par "monitor" sont envoyées à la cible (la blue pill via openOCD).

- `monitor targets`  permet de voir quelle puces est detectée a l'autre bout du stlink (notre blue pill)
- `monitor stm32f1x masse_erase 0` : envoie une commande a un 'stm32f1x' de raz de la memoire  (mass_erase) de la bank 0 (qui contient le programme sur la blue pill)
- `monitor halt` met la puce en pause
- `monitor reset run` reset et démarre la puce


résultat : 

```
gef➤  monitor targets
    TargetName         Type       Endian TapName            State       
--  ------------------ ---------- ------ ------------------ ------------
 0* stm32f1x.cpu       hla_target little stm32f1x.cpu       halted

gef➤  monitor stm32f1x mass_erase 0
stm32x mass erase complete

gef➤  monitor halt
gef➤  monitor reset run
gef➤  

```



## La "cross-compilation"

En temps normal, un code C est compilé via gcc, qui génère un microcode dans le langage de la machine sur laquelle on développe (x86-64 par exemple).  
Pour ça il va notamment utiliser la libc du système qui est liée à l'OS installé et peut lui envoyer des "ordres" (par les syscall notamment).

![](images/scp_012/06_c_normal.png)

Dans notre cas ici, on veut que le code .c qu'on écrira soit compilé pour arm, spécifiquement pour notre blue pill.

Pour cela on utilise arm-none-eabi-gcc  au lieu de gcc.  
Il n'y a pas d'OS sur la blue pill, la libc ne peut donc pas interagir avec lui, cependant on aimerait bien pouvoir bénéficier de l’abstraction qu'elle apporte (malloc, printf, string.h et autres headers).   
Il va falloir adapter la libc, car la glibc 'standard' ne convient pas.    
Il en existe plusieurs pour l'embarqué. Celle qu'on utilise est la "newlib". C'est une libraire standard du C qui présente tout ce qu'il faut, mais qui laisse un peu de travail.  

L'exécutable à la fin de la chaine de compilation (après linkage) est au format ELF, qui défini dans quelles zones charger tel ou tel data. (Heap, BSS, ... voir les C from scratchs d'Imil).  
Hors pour de l'embarqué, chaque puce a ses zones de mémoire bien particulières, il va donc falloir  indiquer "à la main" ou placer quoi. (en vrai on utilisera un linker déjà écrit)

![](images/scp_012/07_cross_compilation.png)

De même, le gdb habituel est fait pour du x86-64, qui n'a pas les mêmes registres, tailles de zones mémoire, et fonctionnement,  que notre blue pill. Pour débugguer le code stm32, il faut donc utiliser un gdb "spécial" qui connait l'architecture de la puce.  
On utilisera donc arm-none-eabi-gdb

### Norme de nommage des logiciels.

Que signifie "arm-none-eabi-gdb" par exemple :

- arm : l'architecture sur laquelle le programme qu'on écrit est censé travailler.
- none : l'OS cible. (ici aucun = none car pas d'OS sur la blue pill, on fait du bear metal). arm-linux-xxx permet de travailler pour un linux déployé sur un arm (comme un raspi par exemple)
- eabi : Extended Application Binary Interface : c'est le gros document qui explique comment "faire les choses" pour faire fonctionner la puce : pour appeler une fonction, le 1er argument est dans le registre R0, le deuxième dans le registre R1, le 3ème dans le R2, les suivants passent par la pile (stack),etc etc etc..
-gdb : l'outil (ici le débuggeur, gcc pour le compilateur C, etc...)


Par exemple pour compiler des choses pour les STM32F4x (qui ont un Float Processing Unit pour les calculs en virgule flottante), on aura arm-hardfloat-eabi-gcc comme compilateur.


### Environnement de programmation pour ARM/STM32

- code perso (.c, .h,...)
- CMSIS  : ensemble de.c et de .h propre à l'architecture ARM. Contient de petites fonctions utiles bas niveau comme NOP (qui ne fait rien),...
- assembleur : sorte de petit bootloader en assembleur, qui démarre dès la mise sous tension de la puce. c'est lui qui va chercher l'exécutable, le mettre en mémoire au bon endroit, ainsi que les éléments à mettre en mémoire ou il faut pour que le programme puisse s'exécuter. puis passe la main au "main" perso.
- linker script : défini à quel endroit de la mémoire, chaque fonction ou élément de donnée, variables, etc... doit être mis. Utilisé pour créer le .elf (ou .bin) après la compilation du code .c en microcode machine.

Le linker script et le petit bootlaoder assembleur sont très liés à l'architecture physique cible. (stm32F1x)

Une fois le .elf ou .bin obtenu (par cross compilation sur le PC), on va pouvoir envoyer le fichier, grâce à openOCD, sur la puce, ou tout sera rangé comme il faut selon ce qui est attendu par l'architecture de la puce.


## CubeMX et génération de projets automatique

CubeMX, application java développée par STMicroelectronics, permet de générer des squelettes de codes pour des projets STM32.  
Il permet donc, entre autres choses pas forcément souhaitées ni utiles, de récupérer l'assembleur et le linker. En plus de certaines librairies ST et de configuration des horloges et des pins.

https://www.st.com/en/development-tools/stm32cubemx.html

Quand on part de rien, c'est intéressant pour démarrer vite, ensuite il faut savoir faire le tri...

Les cartes nucleo ou "officielles" sont connues de cubeMX, les blue pill non... il faut donc choisir le micro controleur plutot que la carte.



### Horloge
Pour un STM32F1x, il est possible d'avoir 
- 2 quartz externes (un à basse fréquence, par exemple 32.768KHz pour la blue pill, et un à plus haute fréquence, 8Mhz sur la bluepill), pour avoir des cycles d'horloges précis
- une horloge interne (circuit RC) peu précise
- une puce dédiée d'horloge envoyé directement au micro-contrôleur  (pas présent sur la blue pill)
En plus de ça, il y a des diviseurs multiplexeurs et multiplicateurs dans la STM32, qui permettent de régler encore plus ces horloges.

Tout ceci se paramètre par des registres bien particulier qu'il faut positionner. CubeMX propose un générateur de config de clock visuel et assez pratique qui peut donner des exemples (ou permettre de ne pas se prendre la tête).


### Led sur PC13

La led de la carte blue pill est branchée sur le port C 13   (PC13), qu'on va positionner en "output" dans cubemx

### chaine de compilation
on choisit qu'on veut compiler avec un makefile

### minimisation de l'utilisation des bib
on choisit de ne pas utiliser la HAL (bibliothèque ST "haut niveau") qui cache tout au développeur et s'abstrait du matériel (on rappelle qu'on fait de l'embarqué, très proche du matériel donc, c'est assez étonnant comme approche)

La bibliothèque LL (low level) elle peut être gardée (elle définie les constantes des registres, etc.)

Ces librairies ne sont pas terribles, on peut utiliser la LL uniquement, mais la HAL est à laisser de coté.

Une fois le projet généré, on trouve une structure de projet... perfectible (répertoires avec des majuscules, HAL quand même présente...) :

A la racine on a 
- le linker (fichier .ld)
- l'assembleur (fichier.s) charger de démarrer la carte
- un makefile complet
- un fichier .ioc qui est le fichier de cubeMX qu'on peut oublier/supprimer

Dans le répertoire "Drivers" on retrouve 
- le CMSIS 
- la HAL (qui a été mise quand même)

dans Inc : 
- les librairies inclues (HAL et LL) 

dans Src :
- les sources du projet


##  Le linker script

pour comprendre le linker script, il nous faut un plan de la mémoire du STM32F1x.

On en trouve un dans la datasheet : [pdf](images/scp_012/STM32F103C8T6_Microcontroller_Schematics_az-delivery.pdf) page 29 ou dans le manuel de référence ST (moins pratique mais plus détaillé)

Dans les micro-contrôleurs, il existe des zones de mémoire spécifiques à telle ou telle fonction.
- La "flash" est de la mémoire de masse (stockage) qui est gardée quand la puce n'est pas alimentée
- la SRAM ou DRAM est de la mémoire vie
- la DMIO sont des zones de mémoire RAM directement reliées à des entrées-sorties. écrire dedans revient à positionner des sorties avec la valeur écrite en mémoire. Entre 2 références de puces, ces mémoires peuvent être positionnée ailleurs.

Une grande partie de la mémoire est "réservée" permettant de mapper des mémoires externes par exemple, ou d'autre fonctionnalités.

### flash et RAM
Le linker défini d'abord les adresses des 2 blocs principaux de mémoire : la flash et la ram  
![](images/scp_012/08_linker_zones_memoire.png)

### sections et positions en mémoire
Et ensuite pour chaque section du fichier ELF (text, rodata, bss, heap,...) il indique ou ils doivent etre rangé en mémoire...

Ci-dessous la section text (le code du programme) est en flash  
![](images/scp_012/09_linker_text_flash.png)

La heap (le tas) elle est en RAM :  
![](images/scp_012/10_linker_heap_ram.png)


### isr_vector


Le isr_vector est une section particulière définie au dessus de text, en haut du linker script. C'est le "vecteur d'interruptions"

![](images/scp_012/11_linker_isr_vector.png)

A l'allumage, la puce ne sait pas quoi faire. Elle cherche donc par défaut dans une zone mémoire bien précise définie par construction, un vecteur qui doit contenir des informations qu'elle attend :
- l'adresse de la fonction à exécuter au démarrage
- la liste des adresses de chaque handler d'interruption

Il existe plus d'une vingtaine de types d'interruptions gérés par la puce. Dès qu'une interruption arrive, une fonction (un handler) est appelé pour traiter l'interruption (le programme principal s'arrête, le temps de traiter l'interruption, avant de reprendre)

Si on définie un interrupt handler spécifique pour une interruption, son adresse sera écrite dans ce vecteur. (sinon un handler générique sera utilisé)

On va retrouver ce vecteur d'interruption dans l'assembleur.

## l'assembleur

![](images/scp_012/12_assembleur_init.png)

Une des section de l'assembleur est le reset_handler qui va initialiser les zones mémoires comme il faut. Par exemple il initialise le BSS (zone des variables non initialisées), avec des 0.  
![](images/scp_012/13_assembleur_bss_zero.png)

Les vecteurs d'interruptions sont tous indiqués avec leur adresse (par exemple DebugMon_Handler)  
![](images/scp_012/14_assembleur_vecteur_interruption.png)

Pour le debugMonHandler, l'adresse à utiliser dans le tableau ci-dessus est soit l'adresse définie dans le label 'DebugMonHandler' si elle existe, sinon on prend l'adresse du default_Handler. C'est un "alias faible".  
![](images/scp_012/15_assembleur_alias_faible_handler.png)


## Template de projet bluepill minimal.

En reprenant uniquement le nécessaire généré par cubeMX, après un gros ménage, il est possible d'avoir un template de projet blue_pill propre contenant le nécessaire et suffisant.  

![](images/scp_012/16_template_blue_pill.png)



L'URL du dépot github de Rancune : https://github.com/Rancunefr/template_bluepill


Au [prochain épisode](scp_ep013.md), compilation du petit programme "blink" du template ci-dessus, upload sur la carte, et explications des fichiers.


----

![fin](images/thats_all_folks_porky.png)











